import {Component} from '@angular/core';
import {HomeComponent} from './home/home.component';
import {Router, RouterLink, RouterOutlet} from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [HomeComponent, RouterLink, RouterOutlet],
  template: `
    <main>
      <a [routerLink]="['/']">
        <header class="brand-name">
        <div style="display: flex; justify-content: space-between;">
          <div style="display: inline-block;">
          <img class="brand-logo" src="/assets/logo.svg" alt="logo" aria-hidden="true" />
          </div>
          <div style="display: inline-block;">
          @if(showLogout){
            <button class="primary" type="button" (click)="logout()" style="cursor:pointer">Logout</button>
          }@else {
            <button class="primary" type="button" (click)="login()" style="cursor:pointer">Login</button>
          }
          </div>
        </div>
          
          
        </header>
      </a>
      <section class="content">
        <router-outlet></router-outlet>
      </section>
    </main>
  `,
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'homes';
  showLogout:boolean=false;
  constructor(private httpClient: HttpClient,private router: Router) {}

  login() {
    window.location.href = '/oauth2/authorization/spring';
  }

  logout() {
    window.location.href = '/logout';
  }

  ngOnInit(): void {
    this.httpClient.get('/api/hello', { responseType: 'text' }).subscribe({
      next: (response) => (this.redirectToHome()),
      error: (error) => (this.redirectToLogin()),
      complete: () => console.info('complete'),
    });
    
  }

  redirectToHome(){
    this.showLogout=true;
    this.router.navigate(['/home']);
  }

  redirectToLogin(){
    this.showLogout=false;
    this.router.navigate(['']);
  }
}
